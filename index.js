var colors = require('colors');
colors.enabled = true;
require("piping")();

(function() {

	var string = '#main/bestpick';
	var matcher = new RegExp(''+
		'^(#(?:(?!\/).)*\/)'+  // #main/ -> $1
		'((?:(?!\/).)*)'       // bestpick -> $2
	);

	var matched = string.match( matcher );
	var replacer = '$1[replaced string]';
	var replaced = string.replace( matcher, replacer );
	console.log('');
	console.log('----------------- 1 ----------------');
	console.log( 'matcher: '.gray, matcher );
	console.log( 'string: '.gray, string );
	console.log( 'replacer: '.gray, replacer );
	console.log( 'matched: '.gray);
	console.log( matched );
	console.log( 'replace result: '.gray, replaced );
})();


(function() {

	var string = '#category/G23423/bestpick';
	var matcher = new RegExp(''+
		'^(#(?:(?!\/).)*\/)'+  // #category/ -> $1
		'((?:(?!\/).)*\/)'+    // #G23423/ -> $2
		'((?:(?!\/).)*)'       // bestpick -> $3
	);

	var matched = string.match( matcher );
	var replacer = '$1$2[replaced string]';
	var replaced = string.replace( matcher, replacer );
	console.log('');
	console.log('----------------- 2 ----------------');
	console.log( 'matcher: '.gray, matcher );
	console.log( 'string: '.gray, string );
	console.log( 'replacer: '.gray, replacer );
	console.log( 'matched: '.gray);
	console.log( matched );
	console.log( 'replace result: '.gray, replaced );
})();
